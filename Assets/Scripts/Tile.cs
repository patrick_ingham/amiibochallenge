﻿using UnityEngine;
using System.Collections;


public class Tile : BoardSquare
{
	public Amiibo amiibo;

	protected override void Start() {
		base.Start ();

		images[1] = amiibo.getAmiiboTexture();
		this.GetComponent<Renderer>().material.mainTexture = images[1];
		textBox.GetComponent<TextMesh>().text = amiibo.getFullName();
	}
	
	protected override void onSquareSelected() {
		base.onSquareSelected();

		Invoke("resetSquare", 1f);
	}

	public override void changeSquareStatus(Color color, string str)
	{
		base.changeSquareStatus(color, str);
	}


	protected override void updateEndGraphic ()
	{
		base.updateEndGraphic();
	}
	
	public void tilePicked()
	{
		base.OnMouseDown();
	}
}